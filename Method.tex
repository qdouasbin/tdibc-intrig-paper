    \subsection{Impedance Modelling\label{subsec:impedance_modelling}}
    \label{subsec:impedance_modelling}
In numerous systems, acoustics plays a major role.
For a large range of combustion systems, from lab experiment to gas turbines or rocket engines, the combustion dynamics is extensively sensitive to acoustic waves.
The propagation of these waves propagation is greatly driven by the impedance $Z$ of the acoustically connected elements such as compressors, turbines, perforated plates, \textit{etc}.
The impedance is defined in the frequency domain as $Z = p / \rho_0 c_0 u$, where $p$ and $u$ are the complex acoustic pressure and velocity, respectivelly, $\rho_0$ the flow density and $c_0$ the sound speed. 
The acoustic waves propagated between two elements can be modelled using impedance. 
Alternatively, the reflection coefficient $R = (Z + 1) / (Z - 1)$ can be used.
A high--fidelity TDIBC should be able to characterize any kind of impedance which can be classified into three categories.
First, simple limit cases such as $R = -1$, $R = 0$, $R = -1$ corresponding to zero acoustic velocity, non--reflecting and zero acoustic pressure conditions, respectively.
Also, experimentally or numerically measured impedance need to be easily modelled.
Finally, any of the above can be associated to a time delay corresponding to the propagation of acoustic waves in a longer geometry as shown in Fig.~\ref{fig:1d_tube}. 
\begin{figure}
    \centering{\includegraphics[width=0.9\textwidth]{tube_acoustics_2parts_02.pdf}
    \caption{   
                Schematic of the 1D acoustic in a tube. 
                $A_{n}^-$ and $A_{n}^+$ are the wave amplitudes in the frequency domain corresponding to the left and right travelling waves in the time--domain in the $n$th part of the tude.
                The reflection coefficient is defined by $R_{n}=A_{n}^- / A_{n}^+$.
            }
    \label{fig:1d_tube}}
\end{figure}
As described in Appendix~\ref{ap:1D_acoustics_appendix} the reflection coefficients $R_2$ (see Fig.~\ref{fig:1d_tube}) in $z = z_2$ can be modelled at location $z_1$ by a so called delayed reflection coefficient $R_{\tau}$ as follows:
    \begin{equation}
       %R_{\tau}\bigg\rvert_{\hat S = \epsilon^{1/n} \frac{s}{\omega_P}} = 
        R_{\tau} = R_2 \vert_{z = z_1} = R_2 e^{- i \omega \frac{2 (z_2 - z_1)}{c_0}} = R_2 e^{- i \omega \tau}
        \label{eq:delayed_reflection_coefficient}
    \end{equation}

Where $R_2$ is the reflection coefficient defined at $z = z_2$ as $R_2 = A_{2}^{-} / A_{2}^{+}$, $R_{\tau} = R_2 \vert_{z = z_1}$ is the reflection coeffcient $R_2$ evaluated at $z =z_1$, $i$ is the imaginary unit and $\omega$ is the pulsation. 
The time delay $\tau$ corresponds to the time for an acoustic wave to propagate back and forth over the distance $z_2 - z_1$ at a speed of sound $c_0$.
The modelling of delayed reflection coefficients as defined in Eq.~\ref{eq:delayed_reflection_coefficient} under the form proposed by Fung et al and Lin and Scalo is extremely challenging.
This challenge arises from the frequency--domain properties of the complex exponential in Eq.~\ref{eq:delayed_reflection_coefficient}.
The main objective of this paper is to create and validate a systematic modelling process to mimic any reflection coefficient. 

%\textcolor{blue}{In the appendix I will derive the equation that lead to the local vs delayed relationships).}

    \subsection{Modelling procedure\label{subsec:modelling_procedure}}

In order to recover the correct Time--Domain behavior of a boundary impendance needs to be modelled.
Given the unbounded nature of the impedance $Z$, it has been prefered to use the wall softness $\widetilde{W} = R + 1$ as a modelling mean of the acoustic impedance (cite). 
As the reflection coefficient $R$ is defined in the interval $ R \in [-1, 1]$, the wall softness $\widetilde{W}$ is positive and defined in $\widetilde{W} \in [0, 2]$.
Equivalently, the reflection coefficient can be used as a modelling mean.
In order to do so the method has to be consistently derived, as presented in Appendix \ref{ap:reflection-based-tdibc-appendix}.
In this work, a fitting procedure has been derived indifferently of the wall softness/reflection coefficient choice. 
In the following, the fitting function $F$ can either represent the wall softness $\widetilde{W}$ or the reflection coeffcient $R$.
In the explicit formulation of a TDIBC proposed by Fung and Ju, the fitting function F is analogous to a damped mass--spring oscillator like system refered it as "acoustic oscillator".
This approach has been validated by \textcolor{blue}{(fung and ju, scaloBodart, that guy from australia doing DNS of airfoils that we saw at MUSAF conference, and dig into citaitons of Scalo + Fung)}.
For a more general use Lin et al. extended the modelling to $n_0$ oscillators, leading to the following fitting function:

    \begin{equation}
        F(\omega) = \sum^{n_0}_{k=1} \left[ \frac{\mu_k}{i \omega - p_k} + \frac{\mu_k^{*}}{i \omega - p_k^{*}} \right]
        \label{eq:fitting_function1}
    \end{equation}

Where $p_k$ and $\mu_k$ denote the pole and residue of the $k$th oscillator, $n_0$ the number of oscillators, $i$ is the imaginary unit, $\omega$ the pulsation and the superscript $^*$ the complex conjugate.
Equation~\ref{eq:fitting_function1} can be recasted by expressing the poles and residues in their algebraic form as $\mu_k = a_k + i b_k$ and $p_k = c_k + i d_k$ and $a_k, b_k, c_k, d_k \in \Bbb R$.
Additionnally, in order to impose a physical near--DC acoustic behavior of the model Lin et al. have shown that $b_k = - a_k c_k / d_k$, leading to:

    \begin{equation}
        F(\omega) =  \sum^{n_0}_{k=1} F_k(\omega) = \sum^{n_0}_{k=1} \frac{2 a_k i \omega}{- \omega^2 - 2 c_k i \omega + \left( c^2_k + d^2_k \right)}
        \label{eq:fitting_function2}
    \end{equation}

\begin{figure}
    \begin{subfigure}[h]{0.5\textwidth}
        \centering
            \includegraphics[width=\textwidth]{single_oscillator_Mod.pdf}
            \caption{Modulus of a single oscillator}
        \label{fig:ModulusSingle}
    \end{subfigure}
    \begin{subfigure}[h]{0.5\textwidth}
        \centering
            \includegraphics[width=\textwidth]{single_oscillator_Phase.pdf}
            \caption{Phase of a single oscillator}
        \label{fig:PhaseSingle}
    \end{subfigure}
    \caption{Modulus (a) and phase (b) of a single oscillator $F_k$ of resonant frequency $f_{0,k} = 100 \textnormal{ Hz}$ for $\mu_k = 350 + 235i$ and $p_k = -350 + 522i$.}
    \label{fig:ExampleSingleOscill}
\end{figure}
Causality is insured for $\Re \left( p \right) = c_k < 0$.
In order to obtain a robust modelling procedure, accurate initial guesses have to be found.
This has been done by deriving algebraic constraints for each degree of freeedom in Eq.~\ref{eq:fitting_function2}.
Figure~\ref{fig:ExampleSingleOscill} shows the modulus and phase of a single oscillator corresponding to one term of the sum in Eq.~\ref{eq:fitting_function2}.
From the damped mass--spring oscillator analogy, it has been shown \textcolor{blue}{(cite carlo)} that the resonant pulsation $\omega_{0,k}$ can be expressed as: 

    \begin{equation}
        \omega_{0, k} = \sqrt{c_k^2 + d_k^2}
        \label{eq:algebraic_constraint2}
    \end{equation}
Leading to:
    \begin{equation}
        d_k = \sqrt{\omega_{0,k}^2 - c_k^2}
        \label{eq:algebraic_constraint3}
    \end{equation}
As shown in Fig.~\ref{fig:ExampleSingleOscill}, at the resonant pulsation $\omega_{0,k}$ a single--oscillator $F_k$ is real valued.
Expressing $F_k$ at the resonant pulsation $\omega_{0,k}$ and using Eq.~\ref{eq:algebraic_constraint3} gives:
    \begin{equation}
        F_k (\omega_{0, k})= h_{0,k} = - \frac{a_k}{c_k}
        \label{eq:constraint_peak_height}
    \end{equation}
Where the peak value at resonance is denoted $h_{0,k}$.
Introducing Eqs.~\ref{eq:algebraic_constraint2} and \ref{eq:constraint_peak_height} in Eq.~\ref{eq:fitting_function2}, we obtain:
    \begin{equation}
        F(\omega) = \sum^{n_0}_{k=1} \frac{  2 h_{0,k}  c_k i \omega}{ \omega^2 + 2 c_k i \omega - \omega_{0,k}^2}
        \label{eq:fitting_function3}
    \end{equation}

\begin{figure}
    \begin{subfigure}[h]{0.5\textwidth}
        \centering
            \includegraphics[width=\textwidth]{Real_part_vs_width.pdf}
            \caption{}
        \label{fig:Real_part_width}
    \end{subfigure}
    \begin{subfigure}[h]{0.5\textwidth}
        \centering
            \includegraphics[width=\textwidth]{Imaginary_part_vs_width.pdf}
            \caption{}
        \label{fig:Imaginary_part_width}
    \end{subfigure}
    \caption{
    Real (a) and imaginary (b) parts of 3 single oscillator models compared to a pure delay. 
    Resonant frequency and peak height constraints (Eqs.~\ref{eq:algebraic_constraint2} and~\ref{eq:constraint_peak_height}) are fullfilled for $f_{0, k} = \frac{1}{2 \pi}\omega_{0,k}  = 500$ Hz and $h_k(\omega_{0,k}) = - a_k / c_k =  1 + 0i$.
    The remaining degree of freedom $c_k$ is varied and its influence on the fitting function width is visualized. 
    The complex exponential is corrrespondong to a delay $\tau = 1$ ms of a reflection coefficient $R=1$.}
    \label{fig:ReImWidth}
\end{figure}

\subsubsection{Single--oscillator width constraint}\label{subsec:sgl_osc_width}

The $c_k$ parameter is the only remaining degree of freedom in Eq.~\ref{eq:fitting_function3}.
Figure~\ref{fig:ReImWidthDelay} highlights the effect of the coefficient $c_k$ on the width of a single--oscillator $F_k$ for a peak height $h_{0,k}=1$ at the resonant frequency $f_{0,k} = \omega_{0,k} / 2 \pi = 500$ Hz. 
The aim of this section is to present the link between $c_k$ and the peak width.
Figure~\ref{subfig:fck_width} presents the real part of a single--oscillator as defined in Eq.~\ref{eq:fitting_function3}.
On can observe that the maximum is located at the resonant pulsation $\omega_{0,k}$.
As $F_k$ is a compact support function its value tends to zero far from the resonance pulsation.
For a given $\epsilon \in [0, h_{0,k}]$ there are two solutions for the equation $\Re(F_k) = \epsilon$.
These solutions are denotated $[\omega_-$,$\omega_+$], with $\omega_- < \omega_{0,k} < \omega_+$.
The function $\Re(F_k)$ is asymetric and decreasing less where $\omega - \omega_{0,k}$ is positive.
The compact support nature of $F_k$ make it suitable for the fit: a small change in $c_k$ has a high impact locally around $\omega = \omega_{0,k}$ and asymptotically tends to zero elsewhere.
In order to improve the robustness of the fitting procedure, one can take advantage of that property by choosing initial conditions that are of low global effect, that is to say to thin oscillators.
Therefore, the width of the peak will be approximated as $\Delta \omega_{c_k} \simeq 2 \Delta \omega_{\epsilon}$.
This approach overestimates the peak width as wanted.


\textcolor{red}{Add up something about $\epsilon$ being between 0 and 1 for $h_{0,k} =1$. That simplifies the width computation.}

Using Eq.~\ref{eq:fitting_function3} and the definition of $\Delta \omega_{\epsilon}$ we know that:

    \begin{equation}
        \Re \left( F_k(\omega_{0,k} + \Delta \omega_{\epsilon}) \right) = \epsilon
        \label{eq:eq_to_solve_for_ck}
    \end{equation}

Soving Eq.~\ref{eq:eq_to_solve_for_ck} for a given width $\Delta \omega_{\epsilon}$ we obtain a constraint on $c_k$:

    \begin{equation}
        c_k(\epsilon, \Delta \omega_{\epsilon}) =  \frac{\omega_{0,k} \Delta \omega_{\epsilon} + \frac{1}{2} \Delta \omega^2_{\epsilon}}{\omega_{0,k} + \Delta \omega_{\epsilon}} \sqrt{\frac{\epsilon}{1 - \epsilon}} 
        \label{eq:eq_for_ck}
    \end{equation}




%Equation~\ref{eq:algebraic_constraint3} is a hard constraint at near--DC frequencies : low resonant frequency will result in imposing low $c_k$ values, leading therefore to a small width.
%However in practice this trouble can be overcome by a slight increase of the number of oscillators $n_0$.
%For a given oscillator the only remaining free parameter in therefore $c_k$.


\begin{figure}
    \begin{subfigure}[h]{0.5\textwidth}
        \centering
            \includegraphics[width=\textwidth]{Real_part_Fk_ck_width.pdf}
            \caption{Width $\Delta \omega_{\epsilon}$ for $\epsilon = 0.2$.}
        \label{subfig:fck_width}
    \end{subfigure}
    \begin{subfigure}[h]{0.5\textwidth}
        \centering
            \includegraphics[width=\textwidth]{Real_part_delay_width.pdf}
            \caption{Width $\Delta f_{\tau}$ for $\tau = 2$ ms.}
        \label{subfig:Real_part_width}
    \end{subfigure}
    \caption{
    Figure~\ref{subfig:fck_width} illustrates the definition of the width $\Delta \omega_{c_k}$ (grey area) defined as $2 \Delta \omega_{\epsilon}$. 
    The value $\epsilon$ is an arbitrary percentage of the peak height $h_{0,k}$.
    In this example $\epsilon = 0.2$.
    As the peak is asymetric we define the total width $\Delta \omega_{c_k} \simeq 2 \Delta \omega_{\epsilon}$.
    Figure~\ref{subfig:Real_part_width} illustrates the definition of the width $\Delta \omega_{\tau}$, corresponding to the frequency range between 2 zeros of an acoustic delay $\tau$. 
    Here, the acoustic time delay $\tau = 2$ ms.
    }
    \label{fig:ReImWidthDelay}
\end{figure}

\subsubsection{Purely Delayed Impedance width constraint}\label{subsec:delayed_imp_width}

As seen in Section~\ref{subsec:impedance_modelling}, a known reflection coefficient $R_2$ of a boundary located at $z = z_2$ can be modelled in another location $z=z_1$.
The modelled reflection coefficient $R_{\tau} = R_2 \vert_{z = z_1}$ is linked to $R_2$ in the frequency--domain through a complex exponential $e^{- i \omega \tau}$.
As $\tau$ is the acoustic time delay of a wave to travel back and forth through the modelled segment $z_2 - z_1$, the $R_{\tau}$ is referred to as "delayed reflection coefficient".
Figure~\ref{subfig:Real_part_width} shows the frequency range $\Delta f_{\tau}$ between two consecutive zeros that can be computed as:

    \begin{equation}
        \Delta \omega_{\tau} = \frac{(n+1)\pi}{\tau} - \frac{n \pi}{\tau} = \frac{\pi}{\tau}  
        \label{eq:eq_om_tau}
    \end{equation}

For the modelling of delay reflection coefficient the width peak $2\Delta \omega_{\epsilon}$ should be lower than $\Delta \omega_{\tau}$, leading to:

    \begin{eqnarray}
        c_k(\epsilon)   &\leq&  \frac{\omega_{0,k} \frac{\Delta \omega_{\tau}}{2} + \frac{1}{2} (\frac{\Delta \omega_{\epsilon}}{2})^2   }{\omega_{0,k} + \frac{\Delta \omega_{\tau}}{2}} \sqrt{\frac{\epsilon}{1 - \epsilon}} 
        \label{eq:eq_ck_tau} \\
                        &\leq& \frac{\frac{\pi}{\tau} \omega_{0,k} +  (\frac{1}{2}\frac{\pi}{\tau})^2   }{2 \omega_{0,k} + \frac{\pi}{\tau}} \sqrt{\frac{\epsilon}{1 - \epsilon}} 
    \end{eqnarray}

    \begin{figure}
        \centering
            \includegraphics[width=0.4\textwidth]{algo_diag_crop_numbered.pdf}
        \caption{
        Algorithm of the iterative fitting.
        }
        \label{fig:algo_diagram}
    \end{figure}

    \subsection{Iterative fit}\label{subsec:iterative_fit}
    High--quality impedance modelling can be acheived by a least--square fit.
    The aim of the modelling is to define ($\mu_k$, $p_k$) in Eq.~\ref{eq:fitting_function1} so that the least--square residual is minimzed.
    Impedance modelling as required can be a tedious task, especially in highly constrainted problems such as delayed impedance.
    If no specific care is taken, the fitting procedure may diverge.
    To improve its robustness three measures where found to be necessary: (1) increase iteratively the number of oscillators, (2) reinitialize the oscillators using fitted values, (3) add an oscillator using the precise algebraic relations described in Sections~\ref{subsec:sgl_osc_width} and \ref{subsec:delayed_imp_width}.
    Figure~\ref{fig:algo_diagram} presents the algorithm used in the fitting procedure.
    At each iteration the modelling consists in 6 steps as described in Fig~\ref{fig:algo_diagram}:
        \begin{itemize}
            \item Step 1 consists in increment the number of oscillators $n_0$ considered in Eq.~\ref{eq:fitting_function2}.
            \item Step 2 consists in expressing the error function on the real part: 
                \begin{equation}
                    E = \Re(R) - \Re(F)
                    \label{eq:error_fnct}
                \end{equation}
            \item Step 3 consists in initalizing the new oscillator as: 
                \begin{equation}
                    h_{0,k} = \max(E) = \Re(R(\omega_{0,k}) - \Re(F(\omega_{0,k}))
                    \label{eq:hk_omk_form_error_fnct}
                \end{equation}
            \item Step 4 consists in initalizing the new oscillator by using the Eqs.~\ref{eq:constraint_peak_height}, \ref{eq:algebraic_constraint3} and Eq.~\ref{eq:eq_for_ck} in the general case and Eq.~\ref{eq:eq_ck_tau} for the delayed case.
            \item Step 5 consists in minimizing the least--square residual $S$ defined on both real and imaginary parts of $R$ and $F$.
            \item Step 6 consists in evaluating if the residual is sufficiently low. The value $S_{\textnormal{min}}$ is a user--defined parameter.


        \end{itemize}
    
